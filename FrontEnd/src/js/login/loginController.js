/* global angular */
var LoginController = function($scope, loginService) {

    $scope.loginSubmit = function(email, password) {
        loginService.login(email, password);
    };
};
angular.module('login', ["collab"])
    .controller('loginController', ['$scope', "loginService", LoginController]);
