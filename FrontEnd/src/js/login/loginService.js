/* global angular */

var LoginService = function(collabService, $log, $window) {
    function loginSuccessHandler() {
        $window.location.href = "#/collab";
    }

    function loginFailureHandler(error) {
        $log.error(error);
    }

    function login(username, password) {
        collabService.getCollab().join(username, password, loginSuccessHandler, loginFailureHandler);
    }

    return {
        login: login
    };

};


angular.module("login")
    .factory("loginService", ["collabService", "$log", "$window", LoginService]);
