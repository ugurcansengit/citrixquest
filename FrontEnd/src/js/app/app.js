/* global angular */

var app = angular.module('app', ['ngRoute', "login", "collab"]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'templates/login/login.html',
            controller: 'loginController'
        }).when('/login', {
            templateUrl: 'templates/login/login.html',
            controller: 'loginController'
        })
        .when('/collab', {
            templateUrl: 'templates/collab/collab.html',
            controller: 'collabController'
        });
}]);
