/* global angular, collabLibrary */

var CollabService = function() {

    var collabInstance = null;

    function logHandler(logMessage) {
        window.console.log(logMessage);
    }

    function init() {
        collabInstance = collabLibrary.init();
        collabLibrary.logManager.initLogging(logHandler);
        return collabInstance;
    }

    return {
        init: init,
        getCollab: function() {
            if (!collabInstance) {
                init();
                return collabInstance;
            }
            return collabInstance;
        }
    };
};

angular.module('collab')
    .factory('collabService', [CollabService]);
