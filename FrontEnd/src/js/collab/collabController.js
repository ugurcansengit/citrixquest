/* global angular */

angular.module('collab', [])
    .controller('collabController', ['$scope', 'collabService', '$filter', function($scope, collabService, $filter) {
        var collabInstance = collabService.getCollab();
        $scope.attendeeBox = [];
        $scope.notifications = [];

        function addParticipant(pId, isActive) {
            $scope.attendeeBox.push({
                pId: pId,
                isActive: isActive
            });
        }

        function popNotification(params) {
            $scope.notifications.push({
                content: params.content,
                type: params.type
            });
        }

        collabInstance.activeSpeakerEvent = function(event) {
            var participantElement = $filter('filter')($scope.attendeeBox, {
                pId: event.user.id
            })[0];
            // activeSpeaker events will be ignored in case of mute
            if (participantElement) {
                if (!participantElement.isMuted) {
                    participantElement.isActive = event.type;
                }
            } else {
                addParticipant(event.user.id, event.type);
            }
            $scope.$apply();
        };

        collabInstance.onMuteStateChanged = function(event) {
            var participantElement = $filter('filter')($scope.attendeeBox, {
                pId: event.user.id
            })[0];
            if (participantElement) {
                participantElement.isMuted = event.muted;
            }
            popNotification({
                content: "User " + event.user.id + " is " + (event.muted ? "muted" : "unmuted"),
                type: "info"
            });
        };

        $scope.muteAction = function(item) {
            if (item.isMuted) {
                collabInstance.unmute(item.pId, function() {}, function(error) {
                    popNotification({
                        content: "Failed to mute/unmute user: " + item.pId,
                        type: "error"
                    });
                });
            } else {
                collabInstance.mute(item.pId, function() {}, function(error) {
                    popNotification({
                        content: "Failed to mute/unmute user: " + item.pId,
                        type: "error"
                    });
                });
            }
        };
    }]);
