(function( window, undefined ) {

var CONSTANTS = {
    NOTIFICATION: {
        EVENT_NAMES: {
            ACTIVE_SPEAKER: "activeSpeaker",
            MUTE_STATE: "muteState",
            ACTIVE: "active",
            INACTIVE: "inactive"
        }
    },
    REQUEST_TYPES: {
        GET: "GET",
        POST: "POST",
        PUT: "PUT",
        DELETE: "DELETE"
    },
    ERRORS: {

    }
};

/*global , __testonly__*/

var Map = function() {
    var items = {},
        length = 0;

    this.size = function() {
        return length;
    };

    this.add = function(key, value) {
        length++;
        items[key] = value;
        return this;
    };

    this.get = function(key) {
        return items[key];
    };

    this.remove = function(key) {
        if (items[key]) {
            length--;
            return delete items[key];
        } else {
            return false;
        }
    };

    this.clear = function() {
        var variableKey;
        for (variableKey in items) {
            if (items.hasOwnProperty(variableKey)) {
                if (delete items[variableKey]) {
                    length--;
                }
            }
        }
    };

    this.entries = function() {
        return items;
    };
};

/* global */

var Utils = function() {

    this.callFunctionIfExist = function() {
        var args = Array.prototype.slice.call(arguments),
            func;
        func = args.shift();
        if (typeof(func) === 'function') {
            try {
                func.apply(null, args);
                return true;
            } catch (e) {
                //logger.error("Exception occured:\n" + e.stack);
                return undefined;
            }
        } else {
            //logger.info("Not a function:" + func);
            return -1;
        }
    };
};

var utils = new Utils();

/* global, utils, CONSTANTS, Map */
var NotificationManagerImpl = function(utils, map, CONSTANTS) {
    var eventLookupTable = {},
        EVENT_NAMES = CONSTANTS.NOTIFICATION.EVENT_NAMES,
        speakerEventMap = new map(),
        ACTIVITY_TIMEOUT_INTERVAL = 1000;

    function fireActiveSpeakerEvent(payload, collabObj, isActive) {
        window.console.log("Firing active speaker event: " + isActive + ", user: " + payload.user.id);
        payload.type = isActive ? EVENT_NAMES.ACTIVE : EVENT_NAMES.INACTIVE;
        utils.callFunctionIfExist(collabObj.public.activeSpeakerEvent, payload);
    }

    function setActivityTimer(speakerObj) {
        speakerObj.activityTimer = window.setTimeout(function() {
            fireActiveSpeakerEvent(speakerObj.payload, speakerObj.collabObj, false);
            speakerObj.activityTimer = undefined;
        }, ACTIVITY_TIMEOUT_INTERVAL);
    }

    function resetActivityTimer(speakerObj) {
        window.clearTimeout(speakerObj.activityTimer);
        setActivityTimer(speakerObj);
    }

    function createActiveSpeakerObj(payload, collabObj) {
        var speakerObj = {
            payload: payload,
            collabObj: collabObj
        };
        speakerEventMap.add(payload.user.id, speakerObj);
        fireActiveSpeakerEvent(speakerObj.payload, speakerObj.collabObj, true);
        setActivityTimer(speakerObj);
    }

    function handleActiveSpeaker(payload, collabObj) {
        var speakerObj = speakerEventMap.get(payload.user.id);
        if (speakerObj) {
            if (speakerObj.activityTimer) {
                window.console.log("activityTimer found, resetting it: " + payload.user.id);
                resetActivityTimer(speakerObj);
            } else {
                window.console.log("object found, setting activityTimer:" + payload.user.id);
                fireActiveSpeakerEvent(payload, collabObj, true);
                setActivityTimer(speakerObj, payload, collabObj);
            }
        } else {
            window.console.log("object found creating it");
            createActiveSpeakerObj(payload, collabObj);
        }
    }

    function handleMuteState(payload, collabObj) {
        utils.callFunctionIfExist(collabObj.public.onMuteStateChanged, payload);
    }

    eventLookupTable[EVENT_NAMES.MUTE_STATE] = handleMuteState;
    eventLookupTable[EVENT_NAMES.ACTIVE_SPEAKER] = handleActiveSpeaker;

    this.handleNotification = function(event, collabObj) {
        if (eventLookupTable[event.name]) {
            eventLookupTable[event.name](event.payload, collabObj);
        } else {
            window.console.log(event);
        }
    };
};

var NotificationManager = function(_utils, _map, _CONSTANTS) {
    return new NotificationManagerImpl(_utils || utils,
        _map || Map,
        _CONSTANTS || CONSTANTS);
};

var notificationManager = new NotificationManager();

/* global, CONSTANTS */
var HttpClientImpl = function(CONSTANTS) {
    var REQUEST_TYPES = CONSTANTS.REQUEST_TYPES,
        API_URL = "http://107.170.105.13/api/";

    function _sendRequest(reqType, url, data, header, isSync, successCallback, failureCallback) {
        var httpRequest = new XMLHttpRequest(),
            reqUrl = API_URL + url;

        httpRequest.onreadystatechange = function() {
            var responseData;
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    responseData = JSON.parse(httpRequest.responseText);
                    if (responseData.status === "ok") {
                        successCallback(responseData.payload);
                    } else {
                        failureCallback();
                    }
                } else {
                    failureCallback(httpRequest.error)
                }
            }
        };

        httpRequest.open(reqType, reqUrl, isSync);
        httpRequest.setRequestHeader("Content-type", "application/json");
        httpRequest.send(JSON.stringify(data));
    }

    this.get = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.GET, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.post = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.POST, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.put = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.PUT, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.delete = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.DELETE, url, data, headers, isSync, successCallback, failureCallback);
    };
};

var HTTPClient = function(_CONSTANTS) {
    return new HttpClientImpl(_CONSTANTS || CONSTANTS);
};

var HTTPClient = new HTTPClient();

/* global, HTTPClient */

var CollabrationServiceImpl = function(HTTPClient) {
    this.join = function(username, password, successCallback, failureCallback) {
        var data = {
                login: username,
                password: password
            },
            url = "session/join";
        HTTPClient.post(url, data, true, successCallback, failureCallback, undefined);
    };

    this.mute = function(attendeeId, token, successCallback, failureCallback) {
        data = {
                token: token
            },
            url = "mute/" + attendeeId
        HTTPClient.put(url, data, false, successCallback, failureCallback, undefined);
    }

    this.unmute = function(attendeeId, token, successCallback, failureCallback) {
        data = {
                token: token
            },
            url = "unmute/" + attendeeId;
        HTTPClient.put(url, data, false, successCallback, failureCallback, undefined);
    }
}

var CollabrationService = function(_HTTPClient) {
    return new CollabrationServiceImpl(_HTTPClient || HTTPClient);
};

var collabrationService = new CollabrationService();

/* global, notificationManager, collabrationService, window*/

var CollabrationManagerImpl = function(notificationManager, collabrationService, window) {

        var collabrationObject = {
                public: {},
                participans: {}
            },
            webSocketConnection,
            onWebSocketSuccess,
            onWebSocketFailure;

        function hasWebSocketConnection() {
            if (webSocketConnection && webSocketConnection.readyState === webSocketConnection.OPEN) {
                return true;
            }
            return false;
        }

        function createWebSocketConnection(wsUrl, token, onSuccess, onFailure) {
            if (hasWebSocketConnection()) {
                webSocketConnection.close();
            }
            try {
                webSocketConnection = new window.WebSocket(wsUrl + "/?token=" + token);
            } catch (exception) {
                onFailure(exception);
                return;
            }
            if (webSocketConnection !== null) {
                webSocketConnection.onmessage = function(event) {
                    var eventData;
                    if (event.data) {
                        eventData = JSON.parse(event.data);
                    }
                    notificationManager.handleNotification(eventData, collabrationObject);
                };
                webSocketConnection.onopen = function() {
                    onSuccess();
                };
                webSocketConnection.onclose = function() {
                    //log
                };
                webSocketConnection.onerror = function() {
                    //log
                };
            } else {
                onFailure();
            }
        }

        function onJoinSuccess(data) {
            collabrationObject.wsUrl = data.rtmUrl;
            collabrationObject.token = data.token;
            collabrationObject.user = data.user;
            collabrationObject.public.user = data.user;

            createWebSocketConnection(data.rtmUrl, data.token, onWebsocketSuccess, onWebsocketFailure);
        }

        function decorateCollabObj() {
            collabrationObject.public.mute = function(attendeeId, successCallback, failureCallback) {
                collabrationService.mute(attendeeId, collabrationObject.token, successCallback, failureCallback)
            };
            collabrationObject.public.unmute = function(attendeeId, successCallback, failureCallback) {
                collabrationService.unmute(attendeeId, collabrationObject.token, successCallback, failureCallback)
            };
        }

        this.init = function() {
            collabrationObject.public.join = function(username, password, successCallback, failureCallback) {
                onWebsocketSuccess = function() {
                    decorateCollabObj();
                    // Returning a collabration object to callback provided by front end
                    successCallback(collabrationObject.public);
                };
                onWebsocketFailure = function(exception) {
                    failureCallback(exception);
                };
                collabrationService.join(username, password, onJoinSuccess, failureCallback);
            };
            return collabrationObject.public;
        };
    },
    collabLibrary;

var collabrationManager = function(_notificationManager, _collabrationService, _window) {
    return new CollabrationManagerImpl(
        _notificationManager || notificationManager,
        _collabrationService || collabrationService,
        _window || window);
};

collabLibrary = new collabrationManager();

window.collabLibrary = collabLibrary;

/*global window */
if ( typeof window.define === "function" && window.define.amd ) {
	define( "collabLibrary", [], function () { return window.collabLibrary; } );
}

})( window );
