# # Citrix Collabration UI - Ugurcan Sengit

## Dependency
### Install nodeJS
<https://nodejs.org/>

## Project Installation
* The project uses GruntJS javascript task runner.
(More about GruntJS <http://gruntjs.com/>)
* Open project folder from command line and run following code to install grunt
```
npm install -g grunt-cli
```
* And then run this code to install grunt dependencies
```
npm install
```

## Build
* Use following command from command line
```
grunt
```

## Usage and Thoughts
* You can login this application with any e-mail address like in x@x.com format
* After you login the application, the application will redirect to "collab" page
* In collab page, users will be allowed to see active speakers and mute/unmute participants by clicking on boxes
* Since I have limited time due to my job, I couldn't handle error scenarios
* You can find a live demo in following address
```
http://apitrial.azurewebsites.net/#/login
```
* You can reach this projects repository at the following address
```
https://bitbucket.org/ugurcansengit/citrixquest/
```
