module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        trimtrailingspaces: {
            main: {
                src: ['src/js/**/*.js'],
                options: {
                    filter: 'isFile',
                    encoding: 'utf8',
                    failIfTrimmed: false
                }
            }
        },
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                forin: true,
                freeze: true,
                noarg: true,
                nocomma: true,
                nonbsp: true,
                nonew: true,
                undef: true,
                futurehostile: true,
                latedef: true,
                maxcomplexity: 5,
                browser: true,
                reporter: 'checkstyle',
                reporterOutput: 'target/jshint-result.xml'
            },
            all: ['src/js/**/*.js']
        },
        clean: ["www"],
        concat: {
            js: {
                src: 'src/js/**/*.js',
                dest: 'www/js/main.js'
            },
            css: {
                src: 'src/css/*.css',
                dest: 'www/css/main.css'
            }
        },
        /*uglify: {
            'www/js/main.min.js': ['www/js/main.js']
        },*/
        copy: {
            img: {
                expand: true,
                src: 'src/img/*',
                dest: 'www/img/',
                flatten: true
            },
            html: {
                cwd: 'src/js/',
                src: '**/*.html',
                dest: 'www/templates/',
                expand: true
            },
            index: {
                expand: true,
                src: ['src/*.html'],
                dest: 'www/',
                filter: 'isFile',
                flatten: true
            },
            library: {
                expand: true,
                src: ['../JSL/target/js/collabLibrary.js'],
                dest: 'www/js',
                filter: 'isFile',
                flatten: true
            }
        },
        cssmin: {
            'www/css/main.min.css': 'www/css/main.css'
        },
        /*
				 *  Not working due to handshake issue with Azure server
				secret: grunt.file.readJSON('secret.json'),
        sftp: {
            azure: {
                files: {
                    "./": ["www/*"]
                },
                options: {
                    path: '/site/wwwroot',
                    host: '<%= secret.host %>',
                    username: '<%= secret.username %>',
                    password: '<%= secret.password %>',
                    srcBasePath: 'www/',
                    showProgress: true
                }
            }
        }*/
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-ssh');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-trimtrailingspaces');

    grunt.registerTask('default', ['trimtrailingspaces', 'jshint', 'clean', 'copy', 'concat' /*, 'uglify'*/ , 'cssmin' /*, 'sftp'*/ ]);

};
