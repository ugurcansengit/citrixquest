# Citrix Collabration API - Ugurcan Sengit

## Dependency
### Install nodeJS
<https://nodejs.org/>

## Project Installation
* The project uses GruntJS javascript task runner.
(More about GruntJS <http://gruntjs.com/>)
* Open project folder from command line and run following code to install grunt
```
npm install -g grunt-cli
```
* And then run this code to install grunt dependencies
```
npm install
```

## Build
* Use following command from command line
```
grunt default
```

## Unit Test Environment
* In order to setup unit test environment download and install Karma with following code;
```
$ npm install karma --save-dev
```
* Install necessary Karma plug-ins.
```
$ npm install karma-jasmine karma-chrome-launcher --save-dev
```
* Install Karma command line interface.
```
$ npm install -g karma-cli
```
* Now you can run Karma from command line with following command;
```
karma start my.conf.js
```

## Use Karma with Grunt
* Download grunt-karma with following code:
```
$ npm install grunt-karma --save-dev
```
* Run following command from command line;
```
grunt tests
```

## More
* In grunt command, firstly, "trimtrailingspaces" task runs in order not to have full dif. because of white spaces
* After jshint checks, unit tests runs and generates a coverage report under the target folder
* You can find last builds coverage report in following url
```
http://apitrial.azurewebsites.net/report-html/index.html
```
* There is more information about front-end a live demo url in FrontEnd/README.md file.

## Thoughts and Others
* Even though I think there are more methods in API you have provided, I have failed to find any. I mostly needed a method to get all active participants in session, in order to map activeSpeaker event identifiers with names.
* In api/session/join request, following JSON input gets error stack trace from server side which should be handled I guess.(Observerd with Postman Rest Client)
```
{
login:"username",
password: "pwd"
```
* As an API, I have returned an object to be handled in collabLibrary.init method. Decorated it afterwards.
* Since I get too many notifications for activeSpeakerEvent, I have decided to make it easier for API user. I send activeSpeakerEvent as true, in first occurance, then if activeSpeakerEvent occurs again for same user in one second, I wait for another one second. If no activeSpeakerEvent occurs for the user in one second, activeSpeakerEvent sent as false.
* I had not enough time to handle and test all failure scenarios.
