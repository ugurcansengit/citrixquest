module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ["target"],
        trimtrailingspaces: {
            main: {
                src: ['src/main/js/**/*.js'],
                options: {
                    filter: 'isFile',
                    encoding: 'utf8',
                    failIfTrimmed: false
                }
            }
        },
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                forin: true,
                freeze: true,
                noarg: true,
                nocomma: true,
                nonbsp: true,
                nonew: true,
                undef: true,
                futurehostile: true,
                latedef: true,
                browser: true,
                reporter: 'checkstyle',
                reporterOutput: 'target/jshint-result.xml'
            },
            all: ['src/main/js/**/*.js', '!src/main/js/wrappers/*.js']
        },
        concat: {
            js: {
                src: ["src/main/js/wrappers/wrapper_start.js",
                    "src/main/js/wrappers/testonly_references_include_start.js",
                    "src/main/js/CONSTANTS.js",
                    "src/main/js/Map.js",
                    "src/main/js/Utils.js",
                    "src/main/js/LogManager.js",
                    "src/main/js/NotificationManager.js",
                    "src/main/js/HTTPClient.js",
                    "src/main/js/CollabrationService.js",
                    "src/main/js/CollabrationManager.js",
                    "src/main/js/wrappers/testonly_references_include_end.js",
                    "src/main/js/wrappers/wrapper_end.js"
                ],
                dest: 'target/js/collabLibrary.js'
            }
        },
        replace: {
            global: {
                src: ['target/js/collabLibrary.js'],
                dest: 'target/js/collabLibrary.js',
                replacements: [{
                    from: /\/\*global.*\*\//g,
                    to: ""
                }]
            }
        },
        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            dev: {
                browsers: ['Chrome'],
                singleRun: false
            },
            prod: {
                browsers: ['PhantomJS'],
                singleRun: true,
                preprocessors: {
                    'target/js/*.js': ['coverage']
                }
            }
        }
        /*,
                uglify: {
                    'target/js/main.min.js': ['www/js/main.js']
                }*/
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-trimtrailingspaces');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('default', ['clean', 'trimtrailingspaces', 'jshint', 'concat', 'karma:prod', 'replace:global' /* , 'uglify'*/ ]);
    grunt.registerTask('tests', ['clean', 'trimtrailingspaces', 'jshint', 'concat', 'karma:dev']);

};
