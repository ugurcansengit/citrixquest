module.exports = function(config) {
    config.set({

        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            'target/js/*.js',
            'src/test/js/**/*.js'
        ],


        // list of files to exclude
        exclude: [],

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'html', 'coverage', 'junit'],

        junitReporter: {
            outputFile: 'target/TESTS-report.xml'
        },

        coverageReporter: {
            // specify a common output directory
            dir: 'target/coverage',
            reporters: [
                {type: 'html', subdir: 'report-html'},
                {type: 'cobertura', subdir: '.', file: 'cobertura.xml'},
                {type: 'lcovonly', subdir: '.', file: 'report-lcovonly.txt'},
                {type: 'teamcity', subdir: '.', file: 'teamcity.txt'},
                {type: 'text', subdir: '.', file: 'text.txt'},
                {type: 'text-summary', subdir: '.', file: 'text-summary.txt'},
                {type: 'text'},
                {type: 'text-summary'}
            ]
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true

    });
};
