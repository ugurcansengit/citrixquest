/* global expect, collabTest */

describe('src.test.js.mapTest', function() {
    var mapMock = null;

    beforeEach(function() {
        mapMock = new collabTest.Map();
    });

    it('should right values', function() {
        expect(mapMock).not.toBe(null);
        expect(mapMock).toEqual(jasmine.any(Object));
    });

    describe('tested add function ', function() {
        var privateMapMock = new collabTest.Map();
        beforeEach(function() {
            privateMapMock.add("mapStringData", "test");
        });

        it('should right values', function() {
            expect(privateMapMock.get("mapStringData")).toBe("test");
        });
    });
    describe('tested size function ', function() {
        var privateMapMock = new collabTest.Map();
        beforeEach(function() {
            privateMapMock.add("mapStringData", "test");
            privateMapMock.add("mapNumberData", 5);
            privateMapMock.add("mapObjectData", {
                test: "test"
            });
        });

        it('should right values', function() {
            expect(privateMapMock.size()).toBe(3);
        });
    });
    describe('tested remove function ', function() {
        var privateMapMock = new collabTest.Map();
        beforeEach(function() {
            privateMapMock.add("mapStringData", "test");
            privateMapMock.add("mapNumberData", 5);
            privateMapMock.add("mapObjectData", {
                test: "test"
            });

            privateMapMock.remove("mapStringData");
        });

        it('should right values', function() {
            expect(privateMapMock.get("mapStringData")).toBeUndefined();
        });
    });
    describe('tested clear function ', function() {
        var privateMapMock = new collabTest.Map();
        beforeEach(function() {
            privateMapMock.add("mapStringData", "test");
            privateMapMock.add("mapNumberData", 5);
            privateMapMock.add("mapObjectData", {
                test: "test"
            });

            privateMapMock.clear();
        });

        it('should right values', function() {
            expect(privateMapMock.get("mapStringData")).toBeUndefined();
            expect(privateMapMock.get("mapNumberData")).toBeUndefined();
            expect(privateMapMock.get("mapObjectData")).toBeUndefined();
        });
    });
    describe('tested entries function ', function() {
        var privateMapMock = new collabTest.Map();
        beforeEach(function() {
            privateMapMock.add("mapStringData", "test");
            privateMapMock.add("mapNumberData", 5);
            privateMapMock.add("mapObjectData", {
                test: "test"
            });
        });

        it('should right values', function() {
            expect(privateMapMock.entries()).toEqual({
                "mapStringData": "test",
                "mapNumberData": 5,
                "mapObjectData": {
                    test: "test"
                }
            });
        });
    });
});
