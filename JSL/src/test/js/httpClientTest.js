/* global expect, collabTest */

describe('src.test.js.httpClientTest', function() {
    var utilsMock,
        windowMock,
        logManagerMock,
        httpClientInstance,
        successCallback = jasmine.createSpy("successCallback"),
        failureCallback = jasmine.createSpy("failureCallback"),
        XMLHttpRequestMock;

    beforeEach(function() {
        XMLHttpRequestMock = {
            onreadystatechange: jasmine.createSpy("onreadystatechange"),
            open: jasmine.createSpy("open"),
            setRequestHeader: jasmine.createSpy("setRequestHeader"),
            send: jasmine.createSpy("send")
        }
        utilsMock = {
            callFunctionIfExist: jasmine.createSpy("callFunctionIfExist")
        };
        windowMock = {
            XMLHttpRequest: function() {}
        };
        logManagerMock = {
            log: jasmine.createSpy("join")
        };
        spyOn(windowMock, "XMLHttpRequest").and.callFake(function() {
            return XMLHttpRequestMock;
        });
        httpClientInstance = collabTest.HTTPClient(undefined, logManagerMock, utilsMock, windowMock);
    });

    it('should right values', function() {
        expect(httpClientInstance).not.toBe(null);
        expect(httpClientInstance).toEqual(jasmine.any(Object));
    });

    it('get function ', function() {
        httpClientInstance.get("dummyUrl", "data", false, successCallback, failureCallback);
        expect(windowMock.XMLHttpRequest).toHaveBeenCalled();
        expect(XMLHttpRequestMock.open).toHaveBeenCalled();
        expect(XMLHttpRequestMock.setRequestHeader).toHaveBeenCalled();
        expect(XMLHttpRequestMock.send).toHaveBeenCalled();
    });

    it('post function ', function() {
        httpClientInstance.post("dummyUrl", "data", false, successCallback, failureCallback);
        expect(windowMock.XMLHttpRequest).toHaveBeenCalled();
        expect(XMLHttpRequestMock.open).toHaveBeenCalled();
        expect(XMLHttpRequestMock.setRequestHeader).toHaveBeenCalled();
        expect(XMLHttpRequestMock.send).toHaveBeenCalled();
    });

    it('put function ', function() {
        httpClientInstance.put("dummyUrl", "data", false, successCallback, failureCallback);
        expect(windowMock.XMLHttpRequest).toHaveBeenCalled();
        expect(XMLHttpRequestMock.open).toHaveBeenCalled();
        expect(XMLHttpRequestMock.setRequestHeader).toHaveBeenCalled();
        expect(XMLHttpRequestMock.send).toHaveBeenCalled();
    });

    it('delete function ', function() {
        httpClientInstance.delete("dummyUrl", "data", false, successCallback, failureCallback);
        expect(windowMock.XMLHttpRequest).toHaveBeenCalled();
        expect(XMLHttpRequestMock.open).toHaveBeenCalled();
        expect(XMLHttpRequestMock.setRequestHeader).toHaveBeenCalled();
        expect(XMLHttpRequestMock.send).toHaveBeenCalled();
    });

    describe("onreadystatechange", function() {
        beforeEach(function() {
            httpClientInstance.get("dummyUrl", "data", false, successCallback, failureCallback);
            XMLHttpRequestMock.responseText = "{\"status\":\"ok\",\"payload\":\"payload\"}";
        });

        it("expect failure handler to be called if status not 200", function() {
            XMLHttpRequestMock.readyState = 4;
            XMLHttpRequestMock.status = 503;
            XMLHttpRequestMock.onreadystatechange();
            expect(utilsMock.callFunctionIfExist).toHaveBeenCalled();
            expect(utilsMock.callFunctionIfExist.calls.first().args[0]).toEqual(failureCallback);
        });

        it("expect failure handler to be called if responseData not OK", function() {
            XMLHttpRequestMock.readyState = 4;
            XMLHttpRequestMock.status = 200;
            XMLHttpRequestMock.responseText = "{\"status\":\"fail\",\"payload\":\"payload\"}";
            XMLHttpRequestMock.onreadystatechange();
            expect(utilsMock.callFunctionIfExist).toHaveBeenCalled();
            expect(utilsMock.callFunctionIfExist.calls.first().args[0]).toEqual(failureCallback);
        });

        it("expect success handler to be called if responseData is OK", function() {
            XMLHttpRequestMock.readyState = 4;
            XMLHttpRequestMock.status = 200;
            XMLHttpRequestMock.onreadystatechange();
            expect(utilsMock.callFunctionIfExist).toHaveBeenCalled();
            expect(utilsMock.callFunctionIfExist.calls.first().args[0]).toEqual(successCallback);
        });
    });
});
