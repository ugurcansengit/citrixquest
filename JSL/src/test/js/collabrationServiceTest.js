/* global expect, collabTest */

describe('src.test.js.collabrationServiceTest', function() {
    var httpClientMock = {
            post: jasmine.createSpy("join"),
            put: jasmine.createSpy("join")
        },
        logManagerMock = {
            log: jasmine.createSpy("join")
        },
        collabrationInstance,
        successCallback = jasmine.createSpy("successCallback"),
        failureCallback = jasmine.createSpy("failureCallback");

    beforeEach(function() {
        collabrationInstance = collabTest.CollabrationService(httpClientMock, logManagerMock);
    });

    it('should right values', function() {
        expect(collabrationInstance).not.toBe(null);
        expect(collabrationInstance).toEqual(jasmine.any(Object));
    });

    it('join function ', function() {
        collabrationInstance.join("username", "password", successCallback, failureCallback);
        expect(httpClientMock.post).toHaveBeenCalled();
    });

    it('mute function ', function() {
        collabrationInstance.mute("username", "password", successCallback, failureCallback);
        expect(httpClientMock.put).toHaveBeenCalled();
    });

    it('unmute function ', function() {
        collabrationInstance.unmute("username", "password", successCallback, failureCallback);
        expect(httpClientMock.put).toHaveBeenCalled();
    });
});
