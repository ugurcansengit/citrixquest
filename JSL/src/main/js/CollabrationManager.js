/*global notificationManager, collabrationService, window, logManager, CONSTANTS, __testonly__*/

var CollabrationManagerImpl = function(notificationManager, collabrationService, window, logManager, CONSTANTS) {

        var collabrationObject = {
                public: {},
                participans: {}
            },
            webSocketConnection,
            onWebSocketSuccess,
            onWebSocketFailure;

        function hasWebSocketConnection() {
            if (webSocketConnection && webSocketConnection.readyState === webSocketConnection.OPEN) {
                return true;
            }
            return false;
        }

        function createWebSocketConnection(wsUrl, token, onSuccess, onFailure) {
            if (hasWebSocketConnection()) {
                webSocketConnection.close();
            }
            try {
                webSocketConnection = new window.WebSocket(wsUrl + "/?token=" + token);
            } catch (exception) {
                onFailure(exception);
                return;
            }
            if (webSocketConnection !== null) {
                webSocketConnection.onmessage = function(event) {
                    var eventData;
                    if (event.data) {
                        eventData = JSON.parse(event.data);
                    }
                    notificationManager.handleNotification(eventData, collabrationObject);
                };
                webSocketConnection.onopen = function() {
                    onSuccess();
                };
                webSocketConnection.onclose = function() {};
                webSocketConnection.onerror = function() {
                    onFailure(CONSTANTS.ERRORS.WEBSOCKET_CONNECTION_FAILED);
                };
            } else {
                onFailure(CONSTANTS.ERRORS.WEBSOCKET_CONNECTION_FAILED);
            }
        }

        function onJoinSuccess(data) {
            collabrationObject.wsUrl = data.rtmUrl;
            collabrationObject.token = data.token;
            collabrationObject.user = data.user;
            collabrationObject.public.user = data.user;

            createWebSocketConnection(data.rtmUrl, data.token, onWebSocketSuccess, onWebSocketFailure);
        }

        function decorateCollabObj() {
            collabrationObject.public.mute = function(attendeeId, successCallback, failureCallback) {
                collabrationService.mute(attendeeId, collabrationObject.token, successCallback, failureCallback);
            };
            collabrationObject.public.unmute = function(attendeeId, successCallback, failureCallback) {
                collabrationService.unmute(attendeeId, collabrationObject.token, successCallback, failureCallback);
            };
        }

        this.init = function() {
            collabrationObject.public.join = function(username, password, successCallback, failureCallback) {
                onWebSocketSuccess = function() {
                    decorateCollabObj();
                    // Returning a collabration object to callback provided by front end
                    successCallback(collabrationObject.public);
                };
                onWebSocketFailure = function(exception) {
                    failureCallback(exception);
                };
                collabrationService.join(username, password, onJoinSuccess, failureCallback);
            };
            return collabrationObject.public;
        };
    },
    collabLibrary;

var CollabrationManager = function(_notificationManager, _collabrationService, _window, _logManager, _CONSTANTS) {
    return new CollabrationManagerImpl(
        _notificationManager || notificationManager,
        _collabrationService || collabrationService,
        _window || window,
        _logManager || logManager,
        _CONSTANTS || CONSTANTS);
};

collabLibrary = new CollabrationManager();
collabLibrary.logManager = logManager;
collabLibrary.ERRORS = CONSTANTS.ERRORS;

if (__testonly__) { __testonly__.CollabrationManager = CollabrationManager; }

window.collabLibrary = collabLibrary;
