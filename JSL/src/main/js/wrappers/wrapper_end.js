/*global window */
if ( typeof window.define === "function" && window.define.amd ) {
	define( "collabLibrary", [], function () { return window.collabLibrary; } );
}

})( window );
