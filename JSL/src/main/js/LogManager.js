/*global utils, __testonly__ */
var LogManagerImpl = function() {
    var logHandler;
    this.initLogging = function(handler) {
        if (handler && typeof logHandler !== 'function') {
            logHandler = handler;
            return true;
        }
        return false;
    };

    this.log = function(logMessage) {
        utils.callFunctionIfExist(logHandler, logMessage);
    };
};

var LogManager = function(_utils) {
    return new LogManagerImpl(_utils || utils);
};

if (__testonly__) { __testonly__.LogManager = LogManager; }

var logManager = new LogManager();
