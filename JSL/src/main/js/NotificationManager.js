/*global utils, CONSTANTS, Map, logManager, __testonly__ */
var NotificationManagerImpl = function(utils, map, CONSTANTS, logManager) {
    var eventLookupTable = {},
        EVENT_NAMES = CONSTANTS.NOTIFICATION.EVENT_NAMES,
        speakerEventMap = new map(),
        ACTIVITY_TIMEOUT_INTERVAL = 1000;

    function fireActiveSpeakerEvent(payload, collabObj, isActive) {
        logManager.log("Firing active speaker event: " + isActive + ", user: " + payload.user.id);
        payload.type = isActive ? EVENT_NAMES.ACTIVE : EVENT_NAMES.INACTIVE;
        utils.callFunctionIfExist(collabObj.public.activeSpeakerEvent, payload);
    }

    function setActivityTimer(speakerObj) {
        speakerObj.activityTimer = window.setTimeout(function() {
            fireActiveSpeakerEvent(speakerObj.payload, speakerObj.collabObj, false);
            speakerObj.activityTimer = undefined;
        }, ACTIVITY_TIMEOUT_INTERVAL);
    }

    function resetActivityTimer(speakerObj) {
        window.clearTimeout(speakerObj.activityTimer);
        setActivityTimer(speakerObj);
    }

    function createActiveSpeakerObj(payload, collabObj) {
        var speakerObj = {
            payload: payload,
            collabObj: collabObj
        };
        speakerEventMap.add(payload.user.id, speakerObj);
        fireActiveSpeakerEvent(speakerObj.payload, speakerObj.collabObj, true);
        setActivityTimer(speakerObj);
    }

    function handleActiveSpeaker(payload, collabObj) {
        var speakerObj = speakerEventMap.get(payload.user.id);
        if (speakerObj) {
            if (speakerObj.activityTimer) {
                logManager.log("activityTimer found, resetting it: " + payload.user.id);
                resetActivityTimer(speakerObj);
            } else {
                logManager.log("object found, setting activityTimer:" + payload.user.id);
                fireActiveSpeakerEvent(payload, collabObj, true);
                setActivityTimer(speakerObj, payload, collabObj);
            }
        } else {
            logManager.log("object not found creating it:" + payload.user.id);
            createActiveSpeakerObj(payload, collabObj);
        }
    }

    function handleMuteState(payload, collabObj) {
        utils.callFunctionIfExist(collabObj.public.onMuteStateChanged, payload);
    }

    eventLookupTable[EVENT_NAMES.MUTE_STATE] = handleMuteState;
    eventLookupTable[EVENT_NAMES.ACTIVE_SPEAKER] = handleActiveSpeaker;

    this.handleNotification = function(event, collabObj) {
        if (eventLookupTable[event.name]) {
            eventLookupTable[event.name](event.payload, collabObj);
        } else {
            window.console.log(event);
        }
    };
};

var NotificationManager = function(_utils, _map, _CONSTANTS, _logManager) {
    return new NotificationManagerImpl(_utils || utils,
        _map || Map,
        _CONSTANTS || CONSTANTS,
        _logManager || logManager);
};

if (__testonly__) { __testonly__.NotificationManager = NotificationManager; }

var notificationManager = new NotificationManager();
