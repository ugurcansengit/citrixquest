/* global __testonly__ */

var Utils = function(logManager) {

    this.callFunctionIfExist = function() {
        var args = Array.prototype.slice.call(arguments),
            func;
        func = args.shift();
        if (typeof(func) === 'function') {
            try {
                func.apply(null, args);
                return true;
            } catch (e) {
                return undefined;
            }
        } else {
            return -1;
        }
    };
};

if (__testonly__) { __testonly__.Utils = Utils; }

var utils = new Utils();
