/*global HTTPClient, logManager, __testonly__ */

var CollabrationServiceImpl = function(HTTPClient, logManager) {
    this.join = function(username, password, successCallback, failureCallback) {
        var data = {
                login: username,
                password: password
            },
            url = "session/join";
        HTTPClient.post(url, data, false, successCallback, failureCallback, undefined);
    };

    this.mute = function(attendeeId, token, successCallback, failureCallback) {
        var data = {
                token: token
            },
            url = "mute/" + attendeeId;
        HTTPClient.put(url, data, true, successCallback, failureCallback, undefined);
    };

    this.unmute = function(attendeeId, token, successCallback, failureCallback) {
        var data = {
                token: token
            },
            url = "unmute/" + attendeeId;
        HTTPClient.put(url, data, true, successCallback, failureCallback, undefined);
    };
};

var CollabrationService = function(_HTTPClient, _logManager) {
    return new CollabrationServiceImpl(_HTTPClient || HTTPClient,
        _logManager || logManager);
};

if (__testonly__) { __testonly__.CollabrationService = CollabrationService; }

var collabrationService = new CollabrationService();
