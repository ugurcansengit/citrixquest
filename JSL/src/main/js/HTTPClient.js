/*global CONSTANTS, logManager, utils, __testonly__ */
var HttpClientImpl = function(CONSTANTS, logManager, utils, window) {
    var REQUEST_TYPES = CONSTANTS.REQUEST_TYPES,
        API_URL = "http://107.170.105.13/api/";

    function _sendRequest(reqType, url, data, header, isSync, successCallback, failureCallback) {
        var httpRequest = new window.XMLHttpRequest(),
            reqUrl = API_URL + url;

        httpRequest.onreadystatechange = function() {
            var responseData;
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    responseData = JSON.parse(httpRequest.responseText);
                    if (responseData.status === "ok") {
                        utils.callFunctionIfExist(successCallback, responseData.payload);
                    } else {
                        utils.callFunctionIfExist(failureCallback, CONSTANTS.ERRORS.NETWORK_ERROR);
                    }
                } else {
                    utils.callFunctionIfExist(failureCallback, httpRequest.error);
                }
            }
        };

        httpRequest.open(reqType, reqUrl, isSync);
        httpRequest.setRequestHeader("Content-type", "application/json");
        httpRequest.send(JSON.stringify(data));
    }

    this.get = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.GET, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.post = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.POST, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.put = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.PUT, url, data, headers, isSync, successCallback, failureCallback);
    };

    this.delete = function(url, data, isSync, successCallback, failureCallback, headers) {
        _sendRequest(REQUEST_TYPES.DELETE, url, data, headers, isSync, successCallback, failureCallback);
    };
};

var HTTPClient = function(_CONSTANTS, _logManager, _utils, _window) {
    return new HttpClientImpl(_CONSTANTS || CONSTANTS,
        _logManager || logManager,
        _utils || utils,
        _window || window);
};

if (__testonly__) { __testonly__.HTTPClient = HTTPClient; }

var HTTPClient = new HTTPClient();
