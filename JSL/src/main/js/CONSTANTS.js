/* global __testonly__*/
var CONSTANTS = {
    NOTIFICATION: {
        EVENT_NAMES: {
            ACTIVE_SPEAKER: "activeSpeaker",
            MUTE_STATE: "muteState",
            ACTIVE: true,
            INACTIVE: false
        }
    },
    REQUEST_TYPES: {
        GET: "GET",
        POST: "POST",
        PUT: "PUT",
        DELETE: "DELETE"
    },
    ERRORS: {
        WEBSOCKET_CONNECTION_CLOSED: 1,
        WEBSOCKET_CONNECTION_FAILED: 2,
        NETWORK_ERROR: 3
    }
};

if (__testonly__) { __testonly__.CONSTANTS = CONSTANTS; }
