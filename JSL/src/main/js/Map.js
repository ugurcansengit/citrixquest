/*global , __testonly__*/

var Map = function() {
    var items = {},
        length = 0;

    this.size = function() {
        return length;
    };

    this.add = function(key, value) {
        length++;
        items[key] = value;
        return this;
    };

    this.get = function(key) {
        return items[key];
    };

    this.remove = function(key) {
        if (items[key]) {
            length--;
            return delete items[key];
        } else {
            return false;
        }
    };

    this.clear = function() {
        var variableKey;
        for (variableKey in items) {
            if (items.hasOwnProperty(variableKey)) {
                if (delete items[variableKey]) {
                    length--;
                }
            }
        }
    };

    this.entries = function() {
        return items;
    };
};

if (__testonly__) { __testonly__.Map = Map; }
